/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 * http://www.wellbole.com
 *
 * @className:com.wellbole.shorturl.ShortUrlGenerater
 * @description: 短地址生成器接口
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年10月1日      李飞              v1.0.0        create
 *
 */
package com.wellbole.shorturl;

/**
 * 
 * 短地址生成器接口
 * @author 李飞
 */
public interface ShortUrlGenerater {
	/**
	 * 依据原始长地址生成 短地址
	 * @param longUrl 原始长地址
	 * @return 短地址生成结果
	 */
	public ShortUrlResult generate(String longUrl);
}
